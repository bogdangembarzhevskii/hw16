package data;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import models.Student;
import models.Teacher;

import java.util.ArrayList;
import java.util.List;

public class University {

    private List<Teacher> teachers;
    private List<Student> students;

    private SessionFactory factory;  //Основа для БД

    public University() {
        this.teachers = new ArrayList<>();
        this.students = new ArrayList<>();

        //Основа для БД

        factory = new Configuration().configure
                ("hibernate.cfg.xml").addAnnotatedClass(Student.class).buildSessionFactory();
        factory = new Configuration().configure
                ("hibernate.cfg.xml").addAnnotatedClass(Teacher.class).buildSessionFactory();
    }

    public void addTeacher(Teacher teacher) {
        teachers.add(teacher);
    }
    public void addStudent(Student student) {
        students.add(student);
    }

    public List<Teacher> getTeachersWithoutStudents() {
        List<Teacher> teachersWithoutStudents = new ArrayList<>();
        for (Teacher teacher : teachers) {
            if (teacher.getStudents().isEmpty()) {
                teachersWithoutStudents.add(teacher);
            }
        }
        return teachersWithoutStudents;
    }

    public List<Student> getStudentsWithoutTeachers() {
        List<Student> studentsWithoutTeachers = new ArrayList<>();
        for (Student student : students) {
            if (student.getTeachers().isEmpty()) {
                studentsWithoutTeachers.add(student);
            }
        }
        return studentsWithoutTeachers;
    }

    public Teacher getTeacherWithStudents(Long teacherId) {
        for (Teacher teacher : teachers) {
            if (teacher.getId().equals(teacherId)) {
                return teacher;
            }
        }
        return null;
    }

    public Student getStudentWithTeachers(Long studentId) {
        for (Student student : students) {
            if (student.getId().equals(studentId)) {
                return student;
            }
        }
        return null;
    }
}