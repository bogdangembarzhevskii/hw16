package models;

import jakarta.persistence.ManyToMany;
import lombok.Getter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

public class Teacher {

    @Getter
    private Long id;

    private String firstName;
    private String lastName;
    private String patronymic;

    @ManyToMany(mappedBy = "teachers") //Для БД
    @Getter
    private List<Student> students;

    public Teacher(Long id, String firstName, String lastName, String patronymic) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.patronymic = patronymic;
        this.students = new ArrayList<>();
    }

    public void addStudent(Student student) {
        students.add(student);
    }

    public String getFIO() {
        return firstName + " " + lastName + " " + patronymic;
    }

}