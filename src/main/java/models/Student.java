package models;

import jakarta.persistence.ManyToMany;
import lombok.Getter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

public class Student {

    @Getter
    private Long id;

    private String firstName;
    private String lastName;
    private String patronymic;


    @ManyToMany(mappedBy = "students") //Для БД
    @Getter
    private List<Teacher> teachers;

    public Student(Long id, String firstName, String lastName, String patronymic) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.patronymic = patronymic;
        this.teachers = new ArrayList<>();
    }

    public void addTeacher(Teacher teacher) {
        teachers.add(teacher);
    }

    public String getFIO() {
        return firstName + " " + lastName + " " + patronymic;
    }

}