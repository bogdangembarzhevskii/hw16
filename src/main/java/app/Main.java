package app;

import data.University;
import models.Student;
import models.Teacher;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        University university = getUniversity();

        //Основная часть кода сделана через Main, так как делал давно.
        //Если нужно, чтобы всё синхронизировалось с БД - сделаю, но в задании об этом не сказано.
        //Hybernate подключён и маппинги настроены, добавлена аннотация ManyToMany.

        System.out.println("Учителя без студентов: ");
        List<Teacher> teachersWithoutStudents = university.getTeachersWithoutStudents();
        for (Teacher teacher : teachersWithoutStudents) {
            System.out.print(teacher.getFIO() + ", ");
        }

        System.out.println("\n");
        System.out.println("Ученики без учителей: ");
        List<Student> studentsWithoutTeachers = university.getStudentsWithoutTeachers();
        for (Student student : studentsWithoutTeachers) {
            System.out.print(student.getFIO() + ", ");

        }

        System.out.println("\n");
        System.out.print("Преподаватель со студентами: ");
        Teacher teacherWithStudents = university.getTeacherWithStudents(1L);
        if (teacherWithStudents != null) {
            System.out.println(teacherWithStudents.getFIO() + ". Его ученики: ");
            for (Student student : teacherWithStudents.getStudents()) {
                System.out.print(student.getFIO() + ", ");
            }
            System.out.println("\n");
        }

        System.out.print("Ученик с преподавателями: ");
        Student studentWithTeachers = university.getStudentWithTeachers(4L);
        if (studentWithTeachers != null) {
            System.out.println(studentWithTeachers.getFIO() + ". Его учителя: ");
            for (Teacher teacher : studentWithTeachers.getTeachers()) {
                System.out.print(teacher.getFIO() + ", ");
            }
            System.out.println("\n");
        }
    }

    private static University getUniversity() {
        University university = new University();

        Teacher teacher1 = new Teacher(1L, "Иван", "Иванов", "Иванович");
        Teacher teacher2 = new Teacher(2L, "Марья", "Петрова", "Ивановна");
        Teacher teacher3 = new Teacher(3L, "Олег", "Гасманов", "Леонидович");

        Student student1 = new Student(4L, "Арсен", "Саргсян", "Арменович");
        Student student2 = new Student(5L, "Богдан", "Богданов", "Богданович");
        Student student3 = new Student(6L, "Мария", "Иванова", "Ивановна");

        university.addTeacher(teacher1);
        university.addTeacher(teacher2);
        university.addTeacher(teacher3);

        university.addStudent(student1);
        university.addStudent(student2);
        university.addStudent(student3);

        // Присваеваем студентов учителям и учителей студентам.
        // Если у учителя нет ученика или у ученика нет учителя,
        // То на выводе они не высветятся.

        teacher1.addStudent(student1);
        teacher1.addStudent(student3);

        student3.addTeacher(teacher1);
        student1.addTeacher(teacher1);

        return university;
    }
}